10 ' STAR.BAS - HOLIDAY STAR PROGRAM
20 ' COPYRIGHT (C) 2022 J.P. WILLIS
30 '
40 ' DESIGNED FOR IBM ADVANCED BASIC
50 ' FOR THE IBM PERSONAL COMPUTER.
60 '
70 ' COMPATIBLE WITH ALL DISPLAY ADAPTERS.
79 '
80 INST$="": CNT%=0
90 WHILE INST$<>"E"
100     GOSUB 2000 'GET NEXT INSTRUCTION
110     IF INST$="S" THEN GOSUB 3000 'SKIP
120     IF INST$="D" THEN GOSUB 4000 'DRAW
130     IF INST$="N" THEN GOSUB 5000 'NEWLINE
140 WEND
150 PRINT "H A P P Y   H O L I D A Y !"
160 END
1999 '
2000 '**SUBROUTINE** READ AN INSTRUCTION 
2010 READ INST$
2020 IF INST$<>"S" AND INST$<>"D" AND INST$<>"N" THEN GOTO 2040 ' NO COUNT
2030 READ CNT%
2040 RETURN
2999 '
3000 '**SUBROUTINE** "SKIP"
3010 FOR I%=1 TO CNT%: PRINT " ";: NEXT I%
3020 RETURN
3999 '
4000 '**SUBROUTINE** "DRAW"
4010 FOR I%=1 TO CNT%: PRINT "*";: NEXT I%
4020 RETURN
4099 '
5000 '**SUBROUTINE** "NEWLINE"
5010 FOR I%=1 TO CNT%: PRINT "": NEXT I%
5020 RETURN
9998 '
9999 ' INSTRUCTIONS
10000 DATA S,5,D,1,S,8,D,1,N,1
10010 DATA S,5,D,2,S,6,D,2,N,1
10020 DATA S,5,D,3,S,4,D,3,N,1
10030 DATA S,5,D,4,S,2,D,4,N,1
10040 DATA S,5,D,10,N,1
10050 DATA D,20,N,1
10060 DATA S,1,D,18,N,1
10070 DATA S,2,D,16,N,1
10080 DATA S,3,D,14,N,1
10090 DATA S,4,D,12,N,1
10100 DATA S,3,D,14,N,1
10110 DATA S,2,D,16,N,1
10120 DATA S,1,D,18,N,1
10130 DATA D,20,N,1
10140 DATA S,5,D,10,N,1
10150 DATA S,5,D,4,S,2,D,4,N,1
10160 DATA S,5,D,3,S,4,D,3,N,1
10170 DATA S,5,D,2,S,6,D,2,N,1
10180 DATA S,5,D,1,S,8,D,1,N,1,E